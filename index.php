<?php
/**
 * The main template file.
 */

defined( 'ABSPATH' ) || die( '403 Forbidden' );

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<?php if ( have_posts() ) :
			if ( is_home() && ! is_front_page() ) : ?>
				<header>
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>
			<?php endif;

			while ( have_posts() ) : the_post();
				get_template_part( 'template-parts/content', get_post_format() );
			endwhile;

			// Previous/next page navigation.
			the_posts_pagination( array(
				'prev_text'          => __( 'Previous page', 'hobo' ),
				'next_text'          => __( 'Next page', 'hobo' ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'hobo' ) . ' </span>',
			) );
		else :
			get_template_part( 'template-parts/content', 'none' );
		endif; ?>
		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_sidebar();

get_footer();
