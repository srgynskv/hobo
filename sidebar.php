<?php
/**
 * The template for the sidebar containing the main widget area.
 */

defined( 'ABSPATH' ) || die( '403 Forbidden' );

if ( is_active_sidebar( 'sidebar-1' )  ) : ?>
	<aside id="secondary" class="sidebar widget-area" role="complementary">
		<?php dynamic_sidebar( 'sidebar-1' ); ?>
	</aside><!-- .sidebar .widget-area -->
<?php endif;
