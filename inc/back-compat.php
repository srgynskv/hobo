<?php
/**
 * Hobo back compat functionality
 *
 * Prevents Hobo from running on WordPress versions prior to 4.4,
 * since this theme is not meant to be backward compatible beyond that and
 * relies on many newer functions and markup changes introduced in 4.4.
 */

defined( 'ABSPATH' ) || die( '403 Forbidden' );

/**
 * Prevent switching to Hobo on old versions of WordPress.
 *
 * Switches to the default theme.
 *
 * @since Hobo 1.0
 */
function hobo_switch_theme() {
	switch_theme( WP_DEFAULT_THEME, WP_DEFAULT_THEME );

	unset( $_GET['activated'] );

	add_action( 'admin_notices', 'hobo_upgrade_notice' );
}
add_action( 'after_switch_theme', 'hobo_switch_theme' );

/**
 * Adds a message for unsuccessful theme switch.
 *
 * Prints an update nag after an unsuccessful attempt to switch to
 * Hobo on WordPress versions prior to 4.4.
 *
 * @since Hobo 1.0
 *
 * @global string $wp_version WordPress version.
 */
function hobo_upgrade_notice() {
	$message = sprintf( __( 'Hobo requires at least WordPress version 4.4. You are running version %s. Please upgrade and try again.', 'hobo' ), $GLOBALS['wp_version'] );
	printf( '<div class="error"><p>%s</p></div>', $message );
}

/**
 * Prevents the Customizer from being loaded on WordPress versions prior to 4.4.
 *
 * @since Hobo 1.0
 *
 * @global string $wp_version WordPress version.
 */
function hobo_customize() {
	wp_die( sprintf( __( 'Hobo requires at least WordPress version 4.4. You are running version %s. Please upgrade and try again.', 'hobo' ), $GLOBALS['wp_version'] ), '', array(
		'back_link' => true,
	) );
}
add_action( 'load-customize.php', 'hobo_customize' );

/**
 * Prevents the Theme Preview from being loaded on WordPress versions prior to 4.4.
 *
 * @since Hobo 1.0
 *
 * @global string $wp_version WordPress version.
 */
function hobo_preview() {
	if ( isset( $_GET['preview'] ) ) {
		wp_die( sprintf( __( 'Hobo requires at least WordPress version 4.4. You are running version %s. Please upgrade and try again.', 'hobo' ), $GLOBALS['wp_version'] ) );
	}
}
add_action( 'template_redirect', 'hobo_preview' );
